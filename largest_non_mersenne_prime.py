import time
start_time = time.time()

print(pow(2, 7830457, mod=10 ** 10  ) * 28433 + 1)
print(f"--- {(time.time() - start_time):.10f} seconds ---" )